#!/usr/bin/env bash

export LC_ALL=C

CURRENT_DIR=$(/bin/pwd)

TEMPERATURE=$(/usr/bin/env python $CURRENT_DIR/temperature.py)

MAC_ADDRESS=$(/sbin/ifconfig | /bin/grep HWaddr | /usr/bin/head -n1 | /usr/bin/awk '{ print $5 }' | /bin/sed 's/://g')
API_KEY=$(/bin/cat $CURRENT_DIR/../conf/api_key)
BASE=$(/bin/cat $CURRENT_DIR/../conf/base)

/usr/bin/curl -u $MAC_ADDRESS:$API_KEY -d "degree_celcius=$TEMPERATURE" -X POST http://$BASE/api/update-temperature

############
# check IP address
############

IP_ADDRESS=$(/bin/cat /srv/smarthomie/conf/ip_address)
CURRENT_IP_ADDRESS=$(/sbin/ifconfig | grep inet | head -n1 | sed 's/:/ /g' | awk '{ print $3 }')

if [ "$IP_ADDRESS" != "$CURRENT_IP_ADDRESS" ]; then
	echo $CURRENT_IP_ADDRESS > /srv/smarthomie/conf/ip_address

	/usr/bin/curl -u $MAC_ADDRESS:$API_KEY -d "ip_address=$CURRENT_IP_ADDRESS" -X POST http://$BASE/api/update-ip-address
fi
