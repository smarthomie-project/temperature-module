#!/usr/bin/env bash

export LC_ALL=C
CURRENT_DIR=$(pwd)

################################################
# Copy module specific config and install
# 	required dependencies
################################################

pip install w1thermsensor

cat $CURRENT_DIR/conf/bootconfig >> /boot/config.txt
cat $CURRENT_DIR/conf/modules >> /etc/modules
