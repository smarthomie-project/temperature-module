#!/usr/bin/env bash

export LC_ALL=C
CURRENT_DIR=$(pwd)

################################################
# run firstchecks.sh
################################################

$CURRENT_DIR/firstchecks.sh $0
if [ $? -ne 0 ]; then
	exit 1
fi

################################################
# Install dependencies
################################################

apt-get update
apt-get --yes upgrade --force-yes
apt-get --yes install curl openssh-server nmap python3.4 jq python-pip ntp --force-yes

################################################
# Copy SSH keys, change port and (re)start SSH
################################################

mkdir /home/pi/.ssh
cat $CURRENT_DIR/conf/authorized_keys >> /home/pi/.ssh/authorized_keys
chown -R pi:pi /home/pi/.ssh/

cat /etc/ssh/sshd_config | sed 's/Port [0-9]*/Port 32156/' > /tmp/smarthomie
mv /tmp/smarthomie /etc/ssh/sshd_config

update-rc.d ssh enable
invoke-rc.d ssh start

service ssh restart

################################################
# Move smarthomie files to /srv/smarthomie
################################################

if [ "$CURRENT_DIR" != "/srv/smarthomie/install" ]; then
	mkdir -p /srv/smarthomie/
	cp -r $CURRENT_DIR/../* /srv/smarthomie/
fi

################################################
# run modulespecific.sh
################################################

$CURRENT_DIR/modulespecific.sh

################################################
# run searchbase.sh
################################################

$CURRENT_DIR/searchbase.sh
if [ $? -ne 0 ]; then
	exit 1
fi

################################################
# Finish and clean up installation
################################################

IP_ADDRESS=$(ifconfig | grep inet | head -n1 | sed 's/:/ /g' | awk '{ print $3 }')
echo "$IP_ADDRESS" > /srv/smarthomie/conf/ip_address

echo "send dhcp-requested-address $IP_ADDRESS;" >> /etc/dhcp/dhclient.conf

cp $CURRENT_DIR/conf/crontab /etc/cron.d/smarthomie

echo
echo "Installation finished. The system will reboot in 10 seconds."
echo

sleep 10
reboot
