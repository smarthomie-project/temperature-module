#!/usr/bin/env bash

export LC_ALL=C
CURRENT_DIR=$(pwd)

################################################
# Search for base
################################################

BASE=$(python $CURRENT_DIR/sendbroadcast.py)
if [ $? -ne 0 ]; then
	exit 1
fi

################################################
# Register module
################################################

MAC_ADDRESS=$(ifconfig | grep HWaddr | head -n1 | awk '{ print $5 }' | sed 's/://g')
curl -d "mac_address=$MAC_ADDRESS" -d "type=temperature" -X POST http://$BASE/api/register-module 2> /dev/null | jq .api_key | sed 's/"//g' > /srv/smarthomie/conf/api_key

echo $BASE > /srv/smarthomie/conf/base

exit 0
