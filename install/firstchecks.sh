#!/usr/bin/env bash

export LC_ALL=C
CURRENT_DIR=$(pwd)

################################################
# Check whether script is executed from
# 	ame folder or not
################################################

if [ "$1" != "./install.sh" ]; then
	echo "This script must be run from the same folder"
	echo "Run this script with the following command: ./install.sh"
	exit 1
fi

################################################
# Check whether script is executed as root
# 	or not
################################################

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root."
   exit 1
fi

exit 0
