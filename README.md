# Temperature Module

## Introduction

Before installing the modules, you have to install the base first!

This is the temperature module for the ds18b20 temperature sensor for SmartHomie.

The install script (temperature-module/install/install.sh) installs all requirements and configuration files.

## Requirements

* Any Debian based distribution (tested with Raspbian Jessie Lite)
* Internet connection
* Raspberry Pi or any other (IoT) device with Pi-like GPIO pins

## Installation

Install as follows:

```
sudo apt-get update
sudo apt-get install git-core

git clone https://gitlab.com/technikerprojekt/temperature-module.git

cd temperature-module/install/
./install.sh
```

## Soldering the sensor on to the GPIO pins

| Output on sensor | GPIO pin | GPIO Description |
|:----------------:|:--------:|:----------------:|
| VCC | #01 | DC Power 3V3 |
| GND | #06 | Ground |
| DATA | #07 | GPIO 04 |

> Please remember to solder a 4.7 kiloohms resistor between the VCC and the DATA cable.
